// home background changing
var i = 1;
var id;
var j;
setInterval(function() {
  if (i == 12) {
    // console.log(id)
    $('#homeBg1').addClass("bg-transition-in");
    $('#homeBg1').removeClass("bg-transition-out");
    i = 0;
  } else {
    if (i == 1) {
      $('#homeBg1').removeClass("bg-transition-in");
    }
    id = '#homeBg' + i
    // console.log(id)
    $(id).addClass("bg-transition-out");
    j = i + 1
    id = '#homeBg' + j
    $(id).removeClass("bg-transition-out");
  }
  i++
}, 4000);
// When the user scrolls the page, execute myFunction
// Get the navbar
var navbar = document.getElementById("navbar");
var story = document.getElementById("story");


$(window).resize(function() {
  if ($(window).width() > 991 ) {
    window.onscroll = function() {setStickyNavbar()};
    console.log('resized')
    navbar.classList.remove("navbar-fixed-top");
  } else {
    navbar.classList.add("navbar-fixed-top");
    console.log('resized small')
  }
});

if ($(window).width() > 991 ) {
  window.onscroll = function() {setStickyNavbar()};
} else {
  navbar.classList.add("navbar-fixed-top");
}
// Get the offset position of the navbar
// var sticky = $(window).height()*0.93;
var sticky = 980;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function setStickyNavbar() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky");
    story.classList.add("story-position");
  } else {
    navbar.classList.remove("sticky");
    story.classList.remove("story-position");
  }
}
// navbar scrollspy animation
$("#navbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      var hash = this.hash;
      $('#navbar-collapse').collapse('hide')
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 400, function(){
      // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
// reserve btn scrollspy animation
$(".reserve-nav-btn").on("click", function(event) {
    // Make sure this.hash has a value before overriding default behavior
    $("html, body").animate(
        {
            scrollTop: $("#reserve").offset().top
        },
        400,
        function() {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = "#reserve";
        }
    );
});

// make reservation model show
$("#make-reservation").on("click", function(event) {
    var form_data = {};
    form_data['name'] = document.forms["reservation-form"]["name"].value;
    form_data['phone'] = document.forms["reservation-form"]["contact-phone"].value;
    form_data['email'] = document.forms["reservation-form"]["email"].value;
    form_data['attendance'] = document.forms["reservation-form"]["count"].value;
    form_data["date"] = document.forms["reservation-form"]["date"].value;
    form_data["time"] = document.forms["reservation-form"]["time"].value;
    form_data["note"] = document.forms["reservation-form"]["note"].value;
    form_data["captcha"] = grecaptcha.getResponse();

    // console.log(form_data);
    if (form_data["name"] == "" || form_data["phone"] == "" || form_data["email"] == "" || form_data["attendance"] == "" || form_data["captcha"] == "") {
        alert("請檢查必填欄位！");
        return false;
    } else {
      console.log(form_data)
        $.ajax({
            url: "controller.php",
            method: "POST",
            data: form_data,
            success: function(result) {
                console.log(result);
                $("#sweetAlertModal").modal("show");
            },
            error: function(result) {
                alert("發生錯誤，請重新整理再試一次！");
                console.log(result);
            }
        });
    }
  });
$('.sweet-alert-modal-close').on('click', function (event) {
    $("html, body").animate(
        {
            scrollTop: $("#home").offset().top
        },
        1,
        function() {
            window.location.reload()
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = "#";
        }
    );
})
//set up datepicker
  var today = new Date();
  var dateEnd = new Date();
  dateEnd.setDate(today.getDate()+180);
  // create today's date string
  var todayDD = today.getDate();
  var todayMM = today.getMonth()+1; //January is 0!
  var todayYYYY = today.getFullYear();
  var todayString = todayYYYY+'-'+ (todayMM > 9 ? '' : '0') + todayMM+'-'+ (todayDD > 9 ? '' : '0') + todayDD;
  // create dateEnd's date string
  var endDD = dateEnd.getDate();
  var endMM = dateEnd.getMonth()+1; //January is 0!
  var endYYYY = dateEnd.getFullYear();
  var dateEndString = endYYYY+'-'+ (endMM > 9 ? '' : '0') + endMM+'-'+ (endDD > 9 ? '' : '0') + endDD;

if ($(window).width() >= 992 ) {
  $.fn.datepicker.dates['en'] = {
      days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      daysMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      today: "Today",
      clear: "Clear",
      format: "mm/dd/yyyy",
      titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
      weekStart: 0
  };
  $('#date').datepicker({
    format: "yyyy/mm/dd",
    endDate: dateEndString,
    startDate: todayString,
    maxViewMode: 2
  });
} else {
  document.getElementById('date').setAttribute('type','date');
  document.getElementById('date').setAttribute('min',todayString);
  document.getElementById('date').setAttribute('max',dateEndString);
}
// handling menu showing
function showMenu (menu) {
  switch (menu) {
    case 'lunch':
      $("#lunch").delay(100).fadeIn();
      // document.getElementById('lunch').style.display = 'block';
      // $("body").css({ 'position': 'fixed' });
      break;
    case 'afternoonTea':
      $("#afternoonTea").delay(100).fadeIn();
      // $("body").css({ 'position': 'fixed' });
      break;
    case 'dinner':
      $("#dinner").delay(100).fadeIn();
      // $("body").css({ 'position': 'fixed' });
      break;
    case 'banquet':
      $("#banquet").delay(100).fadeIn();
      // $("body").css({ 'position': 'fixed' });
      break;
    default:
      console.log('nothing')
  }
}
// bind esc key
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
        closeMenu ('lunch')
        closeMenu ('afternoonTea')
        closeMenu ('dinner')
        closeMenu ('banquet')
    }
});
function closeMenu (menu) {
  switch (menu) {
    case 'lunch':
      $("#lunch").animate({ scrollTop: $("#home").offset().top },100);
      $("#lunch").delay(100).fadeOut();
      // $("body").css({ 'position': 'relative' });
      // document.getElementById('lunch').style.display = 'none';
      break;
    case 'afternoonTea':
      $("#afternoonTea").delay(100).fadeOut();
      // $("body").css({ 'position': 'relative' });
      break;
    case 'dinner':
      $("#dinner").delay(100).fadeOut();
      // $("body").css({ 'position': 'relative' });
      break;
    case 'banquet':
      $("#banquet").delay(100).fadeOut();
      // $("body").css({ 'position': 'relative' });
      break;
    default:

  }
}
