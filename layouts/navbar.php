<nav id="navbar" class="navbar navbar-default">
  <div class="container-fluid" style="padding-left: 0;padding-right: 0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-collapse-menu" align="center" id="navbar-collapse">
      <ul class="nav navbar-nav NotoSerif-Semi navbar-ul">
        <li class="navbar-li"><a href="#story">人文導覽</a></li>
        <li class="navbar-li"><a href="#cuisine">精緻料理</a></li>
        <li class="navbar-li"><a href="#memory">場地租借</a></li>
        <li class="navbar-li"><a href="#information">營業資訊</a></li>
        <li class="navbar-li-reserve">
          <a href="#reserve" class="navbar-reserve-btn reserve-btn">
            用餐訂位
            <img src="img/home/reserve-btn.png" class="img-responsive">
          </a>
        </li>
      </ul>
      <div class="visible-sm visible-xs navbar-collapse-menu-links">
        <div class="btn-group none-bg">
          <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            繁體中文 <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">繁體中文</a></li>
            <li class="disabled"><a href="#">English (建置中)</a></li>
          </ul>
        </div>
        |
        <a href="https://qingtian76.tw/cht/index.php">官網</a>
        |
        <a href="https://www.facebook.com/geo76.tw/"><i class="fab fa-facebook-f"></i></a>
      </div>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>
