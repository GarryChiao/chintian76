<section id="home">
  <div id="homeBg1" class="homeBg" style="background-image:url(img/home/1.jpg); z-index: 600;" alt=""></div>
  <div id="homeBg2" class="homeBg" style="background-image:url(img/home/2.jpg); z-index: 550;" alt=""></div>
  <div id="homeBg3" class="homeBg" style="background-image:url(img/home/3.jpg); z-index: 500;" alt=""></div>
  <div id="homeBg4" class="homeBg" style="background-image:url(img/home/4.jpg); z-index: 450;" alt=""></div>
  <div id="homeBg5" class="homeBg" style="background-image:url(img/home/5.jpg); z-index: 400;" alt=""></div>
  <div id="homeBg6" class="homeBg" style="background-image:url(img/home/6.jpg); z-index: 350;" alt=""></div>
  <div id="homeBg7" class="homeBg" style="background-image:url(img/home/7.jpg); z-index: 300;" alt=""></div>
  <div id="homeBg8" class="homeBg" style="background-image:url(img/home/8.jpg); z-index: 250;" alt=""></div>
  <div id="homeBg9" class="homeBg" style="background-image:url(img/home/9.jpg); z-index: 200;" alt=""></div>
  <div id="homeBg10" class="homeBg" style="background-image:url(img/home/10.jpg); z-index: 150;" alt=""></div>
  <div id="homeBg11" class="homeBg" style="background-image:url(img/home/11.jpg); z-index: 100;" alt=""></div>
  <div id="homeBg12" class="homeBg" style="background-image:url(img/home/12.jpg); z-index: 50;" alt=""></div>
  <div class="row">
    <div class="col-xs-3 col-xs-offset-7 col-sm-3 col-sm-offset-7 home-slogan">
      <img src="img/home/slogan.png" class="img-responsive" style="padding-top:197px; z-index: 700;" alt="">
    </div>
  </div>
</section>
<div class="mobile-seperater hidden-sm"></div>
