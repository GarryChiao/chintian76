<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NZX5XFS');</script>
    <!-- End Google Tag Manager -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/app.css">
    <!-- use fontawesome icons with cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <!-- rippler CSS -->
    <link rel="stylesheet" href="css/rippler.min.css">
    <!-- import fonts from google fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700|Noto+Serif:400,700" rel="stylesheet"> -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>ChinTian</title>
  </head>
  <body data-spy="scroll" data-target="#navbar">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZX5XFS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="w-100">
      <?php include 'layouts/top-fixed.php' ?>
      <?php include 'sec1-home.php' ?>
      <?php include 'layouts/navbar.php' ?>
      <?php include 'sec2-story.php' ?>
      <?php include 'sec3-cuisine.php' ?>
      <?php include 'sec4-memory.php' ?>
      <?php include 'sec5-reserve.php' ?>
      <?php include 'sec6-information.php' ?>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script language="JavaScript" type="text/javascript" src="js/jquery.min.js"></script>
    <!-- chintian font family -->
    <script src="//s3-ap-northeast-1.amazonaws.com/justfont-user-script/jf-55687.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/popper.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- rippler js -->
    <script src="js/jquery.rippler.min.js"></script>
    <script type="text/javascript">
    // rippler js
    $(document).ready(function() {
      $(".rippler").rippler({
        effectClass      :  'rippler-effect'
        ,effectSize      :  16      // Default size (width & height)
        ,addElement      :  'div'   // e.g. 'svg'(feature)
        ,duration        :  500
      });
    });
    </script>
    <script language="JavaScript" type="text/javascript" src="js/datepicker.js"></script>
    <script language="JavaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/app.js"></script>
  </body>
</html>
