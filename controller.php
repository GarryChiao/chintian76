<?php
session_start();

require("mysql_connect.php");
require('PHPMailerAutoload.php');

// inser data into mysql
$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$attendance = $_POST['attendance'];
$date = $_POST["date"];
$time = $_POST["time"];
$note = $_POST["note"];
$captcha = $_POST["captcha"];

$verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$RECAPTCHA_SECRET."&response=".$captcha);
$captcha_success=json_decode($verify);
// echo $captcha_success->success;
if($captcha_success->success==false) {
  header('HTTP/1.1 500 Internal Server Error: recaptcha failed.');
  header('Content-Type: application/json; charset=UTF-8');
  json_encode(array('message' => 'google recaptcha failed', 'code' => 500));
} else {

  $sql = "CREATE TABLE IF NOT EXISTS reservation (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  attendance VARCHAR(255) NOT NULL,
  date VARCHAR(255) NOT NULL,
  time VARCHAR(255) NOT NULL,
  note longtext NOT NULL,
  created_at timestamp NOT NULL
  )";

  if ($conn->query($sql) === TRUE) {
      echo "Table reservation created successfully \r\n";
  } else {
      echo "Error creating table: " . $conn->error;
  }

  $sql = "insert into reservation (name, phone, email, attendance, date, time, note, created_at) values('".$name."','".$phone."','".$email."','".$attendance."','".$date."','".$time."','".$note."','".date('Y-m-d H:i:s')."')";

  mysqli_set_charset($conn, "utf8");
  // echo $sql;
  if ($conn->query($sql) === TRUE) {
      echo "New record created successfully \r\n";
  } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
  }

  $mail = new PHPMailer;
  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = '';                 // SMTP username
  $mail->Password = '';                           // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  $mail->setFrom('chintian@gmail.com', 'Chintian');
  $mail->addAddress($mailto, '');     // Add a recipient

  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = '您有新的訂位！';
  // $mime_boundary = md5(uniqid(mt_rand(), TRUE));

  // $content = "This is a multi-part message in MIME format.\r\n";
  // $content .= "--$mime_boundary\r\n";
  // $content .= "Content-Type: text/html; charset=utf-8\r\n";
  // $content .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
  $content = "<div>";
  $content .= "<ul>";
  $content .= "<li> 姓名：".$name." </li>\r\n";
  $content .= "<li> 聯絡電話：".$phone." </li>\r\n";
  $content .= "<li> 電子郵件：".$email." </li>\r\n";
  $content .= "<li> 人數：".$attendance." </li>\r\n";
  $content .= "<li> 日期：".$date." </li>\r\n";
  $content .= "<li> 時段：".$time." </li>\r\n";
  $content .= "<li> 備註：".$note." </li>\r\n";
  $content .= "</ul>";
  $content .= "</div>";

  $mail->Body = $content;
  // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  if(!$mail->send()) {
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
      echo 'Message has been sent';
  }

}
