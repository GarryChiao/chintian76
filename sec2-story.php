<!--Story section-->
<section id="story">
  <div class="row section-row">
    <!-- <div class="col-sm-5 col-sm-offset-2"> -->
    <div class="left-carousel">
      <div id="carousel-example-story" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-story" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-story" data-slide-to="1"></li>
          <li data-target="#carousel-example-story" data-slide-to="2"></li>
          <li data-target="#carousel-example-story" data-slide-to="3"></li>
          <li data-target="#carousel-example-story" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="img/story/slide-1.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/story/slide-2.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/story/slide-3.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/story/slide-4.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/story/slide-5.jpg" alt="...">
          </div>
        </div>
      </div>
    </div>
    <!-- the information part -->
    <!-- <div class="col-sm-5"> -->
    <div class="right-information col-md-5 col-md-offset-7" align="center">
      <div class="">
        <div class="col-md-4 col-md-offset-4 col-sm-12 col-sm-offset-0 information-icon">
          <img src="img/story/navigate-icon.svg" class="img-responsive" alt="">
        </div>
      </div>
      <div class="information-title NotoSerif-Semi">
        <h1>老&nbsp;屋•新&nbsp;生</h1>
      </div>
      <div class="information-description NotoSerif-Medium" align="left">
      <!-- <div class="information-description sourcehansans-tc-regular" align="left"> -->
        <h2>封存時光的日式老木屋，重新打開大門，讓時間再次流動，歡迎進來聽我們講述這些人那些事，一起探尋屬於老台北的文化記憶。</h2>
      </div>
      <div class="information-description-icon-div" align="center">
        <div class="information-description-icon">
          <button type="button" class="information-description-icon" onclick="window.location.href='https://qingtian76.tw/cht/guid.php?type=1'">
            預約導覽
            <i class="fas fa-chevron-circle-right"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
</section>
