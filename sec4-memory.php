<!--Memory section-->
<section id="memory">
  <div class="row section-row">
    <!-- the carousel part -->
    <div class="left-carousel">
      <div id="carousel-example-memory" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-memory" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-memory" data-slide-to="1"></li>
          <li data-target="#carousel-example-memory" data-slide-to="2"></li>
          <li data-target="#carousel-example-memory" data-slide-to="3"></li>
          <li data-target="#carousel-example-memory" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="img/memory/slide-1.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/memory/slide-2.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/memory/slide-3.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/memory/slide-4.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/memory/slide-5.jpg" alt="...">
          </div>
        </div>
      </div>
    </div>
    <!-- the information part -->
    <div class="right-information col-md-5 col-md-offset-7" align="center">
      <div class="">
        <div class="col-md-4 col-md-offset-4 col-sm-12 col-sm-offset-0 information-icon">
          <img src="img/memory/navigate-icon.svg" class="img-responsive" alt="">
        </div>
      </div>
      <div class="information-title NotoSerif-Semi">
        <h1>遙&nbsp;想•延&nbsp;續</h1>
      </div>
      <div class="information-description NotoSerif-Medium" align="left">
        <h2>過去老屋伴著足立家與馬家走過許多人生重要時刻，現在這裡開放給民眾舉辦適合的私人活動，您想和老屋一起創造什麼樣的難忘回憶？</h2>
      </div>
      <div class="information-description-icon-div" align="center">
        <div class="information-description-icon">
          <button type="button" class="information-description-icon button-left" onclick="window.location.href='https://qingtian76.tw/cht/restaurant_space.php'">
            空間一覽
            <i class="fas fa-chevron-circle-right"></i>
          </button>
          <button type="button" class="information-description-icon button-right" onclick="window.location.href='https://qingtian76.tw/cht/rent.php'">
            租借申請
            <i class="fas fa-chevron-circle-right"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- <div class="col-sm-5">
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4 col-sm-3 col-sm-offset-3 information-icon">
          <img src="img/memory/navigate-icon.svg" class="img-responsive" alt="">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-xs-offset-3 col-sm-offset-2 information-title NotoSerif-Semi">
          <h1>遙想•延續</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-1 information-description NotoSerif-Medium">
          <h2>過去老屋伴著足立家與馬家走過許多人生重要時刻，現在這裡開放給民眾舉辦適合的私人活動，您想和老屋一起創造什麼樣的難忘回憶？</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9 information-description-icon-div">
          <div class="col-xs-6 col-sm-5 information-description-icon" align="center">
            <button type="button" class="information-description-icon">
              空間一覽
              <i class="fas fa-chevron-circle-right"></i>
            </button>
          </div>
          <div class="col-xs-6 col-sm-5 information-description-icon" align="center">
            <button type="button" class="information-description-icon reserve-nav-btn">
              租借申請
              <i class="fas fa-chevron-circle-right"></i>
            </button>
          </div>
        </div>
      </div>
    </div> -->
  </div>
</section>
